import DashboardComponent from "@/components/dashboardComponent";
import HomePage from "@/components/HomePage";
import SideBar from "@/components/sideBar";

import React, { useState } from "react";
import Presmetki from "@/components/Presmetki";
import Artikli from "@/components/Artikli";
import Partneri from "@/components/Partneri";
import VlezniFakturi from "@/components/VlezniFakturi";
import IzlezniFakturi from "@/components/IzlezniFakturi";

const dashboard = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [page, setPage] = useState("home");

  return (
    <div className="bg-gray-100 h-screen overflow-scroll">
      <SideBar setPage={setPage} />
      <div className="ms-72">
        {page === "home" ? (
          <HomePage />
        ) : page === "vlezni" ? (
          <VlezniFakturi />
        ) : page === "izlezni" ? (
          <IzlezniFakturi />
        ) : page === "presmetki" ? (
          <Presmetki />
        ) : page === "artikli" ? (
          <Artikli />
        ) : page === "partneri" ? (
          <Partneri />
        ) : null}
      </div>
    </div>
  );
};

export default dashboard;
