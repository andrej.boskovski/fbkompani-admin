import Image from "next/image";
import { Inter } from "next/font/google";
import LoginForm from "@/components/loginForm";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  return (
    <div
      style={{
        backgroundImage: `url("/images/loginBackground.png")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
      }}
      className="h-screen flex justify-center items-center"
    >
      <LoginForm />
    </div>
  );
}
