import React from "react";
import NewFeature from "./NewFeature";

const Artikli = () => {
  return (
    <div>
      <div className="m-5 font-bold text-3xl">Артикли</div>
      <hr className="border border-gray-300 w-12/12" />
      <br />
      <NewFeature />
    </div>
  );
};

export default Artikli;
