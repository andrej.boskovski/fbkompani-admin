import React from "react";
import NewFeature from "./NewFeature";

const VlezniFakturi = () => {
  return (
    <div>
      <div className="m-5 font-bold text-3xl">Влезни фактури</div>;
      <NewFeature />
    </div>
  );
};

export default VlezniFakturi;
