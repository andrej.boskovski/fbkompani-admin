/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from "react";
import Input from "./input";
import Image from "next/image";
import router from "next/router";

const loginForm = () => {
  const users = [
    {
      name: "Andrej",
      lastName: "Boshkovski",
      userName: "andrej",
      password: "123",
    },
    {
      name: "Stole",
      lastName: "Boshkovski",
      userName: "stole",
      password: "123",
    },
  ];

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        const foundProfile = users.find(
          (profile: any) =>
            profile.userName === username && profile.password === password
        );

        if (foundProfile) {
          router.push("dashboard");
          setUsername("");
          setPassword("");
        } else {
          alert("No profile found");
        }
      }}
      className="p-5 py-20  bg-white flex flex-col justify-center items-center rounded-xl shadow-2xl w-3/12"
    >
      <div className="mb-5">
        <Image
          src={"/images/fb-logo.png"}
          alt={"Logo"}
          width={100}
          height={100}
        />
      </div>
      <div className="flex flex-col  w-10/12">
        <Input
          type={"username"}
          placeholder={"Корисничко име"}
          content={username}
          setContent={setUsername}
        />
        <Input
          type={"password"}
          placeholder={"Лозинка"}
          content={password}
          setContent={setPassword}
        />
      </div>
      <input
        type="submit"
        value={"Логирај се!"}
        className="bg-blue-500 text-white px-5 py-2 my-3 rounded-xl"
      />

      <small className="text-blue-500">Заборавена Лозинка?</small>
    </form>
  );
};

export default loginForm;
