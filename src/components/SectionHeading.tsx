import React from "react";

interface Props {
  content: string;
}

const SectionHeading = ({ content }: Props) => {
  return (
    <div className="bg-gray-600 text-white rounded-t-lg py-0.5">
      <p className="px-3 font-bold">{content}</p>
    </div>
  );
};

export default SectionHeading;
