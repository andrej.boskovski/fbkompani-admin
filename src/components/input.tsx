import React from "react";

interface Props {
  type: string;
  placeholder?: string;
  content?: string;
  setContent?: (value: string) => void;
  className?: string;
}

const input = ({
  type,
  placeholder,
  content,
  setContent,
  className,
}: Props) => {
  return (
    <input
      className={`px-3 py-2 m-2 w-12/12 h-10 border border-gray-400 rounded-lg ${className}`}
      type={type}
      placeholder={placeholder}
      value={content}
      onChange={(e) => (setContent ? setContent(e.target.value) : null)}
    />
  );
};

export default input;
