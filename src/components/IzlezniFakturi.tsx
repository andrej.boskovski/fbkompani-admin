import React from "react";
import NewFeature from "./NewFeature";

const IzlezniFakturi = () => {
  return (
    <div>
      <div className="m-5 font-bold text-3xl">Излезни фактури</div>;
      <NewFeature />
    </div>
  );
};

export default IzlezniFakturi;
