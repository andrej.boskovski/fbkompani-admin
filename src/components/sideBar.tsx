import React, { useState } from "react";
import Image from "next/image";
import router from "next/router";

interface Props {
  setPage: (page: string) => void;
}

const sideBar = ({ setPage }: Props) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isFinansiiOpen, setIsFinansiiOpen] = useState<boolean>(false);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [activeLink, setActiveLink] = useState<string>("");

  return (
    <div className="w-2/12 h-screen rounded-e-3xl bg-white shadow-xl fixed">
      <div>
        <Image
          className="m-5 mb-10"
          src={"/images/fb-logo.png"}
          alt={"Logo"}
          width={80}
          height={80}
        />
      </div>
      <ul className="flex flex-col gap-y-3 items-center">
        <li
          className={`border-b-2 w-10/12 cursor-pointer ${
            activeLink === "home" ? "font-bold" : ""
          }`}
          onClick={() => {
            setPage("home");
            setActiveLink("home");
          }}
        >
          Почетна страна
        </li>
        <li
          className={`border-b-2 w-10/12 cursor-pointer ${
            activeLink === "presmetki" ? "font-bold" : ""
          }`}
          onClick={() => {
            setPage("presmetki");
            setActiveLink("presmetki");
          }}
        >
          Пресметки
        </li>
        {/* Finansii */}
        <li className=" border-b-2 w-10/12 cursor-pointer">
          <div
            className="flex"
            onClick={() => {
              isFinansiiOpen
                ? setIsFinansiiOpen(false)
                : setIsFinansiiOpen(true);
            }}
          >
            <p
              className={`mr-1 ${
                activeLink === "vlezni"
                  ? "font-bold"
                  : "" || activeLink === "izlezni"
                  ? "font-bold"
                  : ""
              }`}
            >
              Финансии
            </p>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 320 512"
              width={10}
            >
              <path d="M137.4 374.6c12.5 12.5 32.8 12.5 45.3 0l128-128c9.2-9.2 11.9-22.9 6.9-34.9s-16.6-19.8-29.6-19.8L32 192c-12.9 0-24.6 7.8-29.6 19.8s-2.2 25.7 6.9 34.9l128 128z" />
            </svg>
          </div>
          <ul
            className={`${
              isFinansiiOpen ? "block" : "hidden"
            } list-disc ms-5 flex flex-col gap-y-2 mt-3`}
          >
            <li
              onClick={() => {
                setPage("vlezni");
                setActiveLink("vlezni");
              }}
              className={`border-b-2 pb-2 w-10/12 cursor-pointer ${
                activeLink === "vlezni" ? "font-bold" : ""
              }`}
            >
              Влезни фактури
            </li>
            <li
              className={`mb-2 ${activeLink === "izlezni" ? "font-bold" : ""}`}
              onClick={() => {
                setPage("izlezni");
                setActiveLink("izlezni");
              }}
            >
              Излезни фактури
            </li>
          </ul>
        </li>
        {/* Finansii end */}
        <li
          onClick={() => {
            setPage("partneri");
            setActiveLink("partneri");
          }}
          className={`border-b-2 w-10/12 cursor-pointer ${
            activeLink === "partneri" ? "font-bold" : ""
          }`}
        >
          Партнери
        </li>
        <li
          className={`border-b-2 w-10/12 cursor-pointer ${
            activeLink === "artikli" ? "font-bold" : ""
          }`}
          onClick={() => {
            setPage("artikli");
            setActiveLink("artikli");
          }}
        >
          Артикли
        </li>
      </ul>
      <div
        className={`absolute bottom-10 left-5 text-red-700 cursor-pointer border-b-2 w-10/12 `}
        onClick={() => {
          router.push("/");
        }}
      >
        Исклучи се!
      </div>
    </div>
  );
};

export default sideBar;
