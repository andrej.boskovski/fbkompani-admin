import React from "react";
import NewFeature from "./NewFeature";

const Partneri = () => {
  return (
    <div>
      <div className="m-5 font-bold text-3xl">Партнери</div>;
      <NewFeature />
    </div>
  );
};

export default Partneri;
