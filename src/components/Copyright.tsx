import React from "react";

const Copyright = () => {
  return (
    <footer className="absolute bottom-5 w-full text-center text-gray-400 font-medium">
      All rights reserved <br />
      FB KOMPANI 2024
    </footer>
  );
};

export default Copyright;
