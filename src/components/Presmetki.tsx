import React, { useState } from "react";
import Input from "./input";
import Select from "./Select";
import SectionHeading from "./SectionHeading";

interface Transaction {
  income: string;
  incomeSum: string;
  incomeDate: string;
  type: string;
  category: string;
}

const Presmetki = () => {
  const [income, setIncome] = useState("");
  const [incomeSum, setIncomeSum] = useState("");
  const [incomeDate, setIncomeDate] = useState("");
  const [type, setType] = useState("");
  const [category, setCategory] = useState("");
  const [data, setData] = useState<Transaction[]>([]);

  // Submit
  const handleSubmit = (event: any) => {
    event.preventDefault();

    // Check if all fields are filled
    if (!income || !incomeSum || !incomeDate || !type || !category) {
      alert("Please fill all fields");
      return;
    }

    // Create a new transaction object
    const newTransaction = {
      income,
      incomeSum,
      incomeDate,
      type,
      category,
    };

    // Update the data array with the new transaction
    setData([...data, newTransaction]);

    // Clear inputs
    setIncome("");
    setIncomeSum("");
    setIncomeDate("");
    setType("");
    setCategory("");
  };

  // Helper function to format numbers with commas
  const formatNumberWithCommas = (number: number): string => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  // Calculate the total sum
  const totalSum = data.reduce((total, transaction) => {
    const amount = parseFloat(transaction.incomeSum);
    return transaction.type === "Влез" ? total + amount : total - amount;
  }, 0);

  const handleDelete = (index: number) => {
    setData(data.filter((_, i) => i !== index));
  };

  return (
    <div>
      <div className="mx-5 mt-7 mb-3  font-bold text-3xl">Пресметки</div>
      <hr className="border border-gray-200 w-12/12" />
      {/* Income Form */}
      <form onSubmit={handleSubmit} className="m-3 bg-white w-100 rounded-xl">
        <SectionHeading content="Внеси трансакција" />
        <div className="p-3">
          <Input
            className="w-2/12"
            type={"text"}
            placeholder={"Опис"}
            content={income}
            setContent={setIncome}
          />
          <Input
            className="w-2/12"
            type={"number"}
            placeholder={"Сума"}
            content={incomeSum}
            setContent={setIncomeSum}
          />
          <Input
            type={"date"}
            content={incomeDate}
            setContent={setIncomeDate}
            className="text-gray-400 w-2/12"
          />
          <Select
            className="text-gray-400 w-2/12"
            placeholder="Тип"
            option1="Влез"
            option2="Трошок"
            setSelectedOption={setType}
          />
          <Select
            className="text-gray-400 w-2/12"
            placeholder="Категорија"
            option1="Фирма"
            option2="Столе"
            option3="Горан"
            option4="Транспорт"
            option5="Дневен промет"
            setSelectedOption={setCategory}
          />
          <input
            type="submit"
            value={"Потврди"}
            className="bg-gray-600 text-white px-5 h-10 py-2 my-3 ml-2 rounded-lg"
          />
        </div>
      </form>

      {/* Table */}
      <div className="m-3 bg-white w-100 rounded-xl">
        <SectionHeading content="Трансакции" />
        <div className="relative overflow-x-auto ">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500">
            <thead className="text-xs text-gray-700 uppercase bg-gray-200">
              <tr>
                <th scope="col" className="px-6 py-3">
                  Опис
                </th>
                <th scope="col" className="px-6 py-3">
                  Сума
                </th>
                <th scope="col" className="px-6 py-3">
                  Дата
                </th>
                <th scope="col" className="px-6 py-3">
                  Тип
                </th>
                <th scope="col" className="px-6 py-3">
                  Категорија
                </th>
                <th scope="col" className="px-6 py-3">
                  Промена
                </th>
              </tr>
            </thead>
            <tbody>
              {data.map((transaction, index) => (
                <tr key={index} className="bg-white">
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap"
                  >
                    {transaction.income}
                  </th>
                  <td
                    className={`px-6 py-4 ${
                      transaction.type === "Влез"
                        ? "text-green-500"
                        : "text-red-500"
                    }`}
                  >
                    {transaction.type === "Влез" ? "+ " : "- "}
                    {formatNumberWithCommas(
                      parseFloat(transaction.incomeSum)
                    )}{" "}
                    МКД
                  </td>
                  <td className="px-6 py-4">
                    {new Date(transaction.incomeDate).toLocaleDateString(
                      "mk-MK"
                    )}
                  </td>

                  <td className="px-6 py-4">{transaction.type}</td>
                  <td className="px-6 py-4">{transaction.category}</td>
                  <td className="px-6 py-4">
                    <button
                      className="text-red-500 underline"
                      onClick={() => handleDelete(index)}
                    >
                      Избриши
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot className="bg-gray-200 rounded-b-xl">
              <tr className="font-semibold text-gray-900">
                <th scope="row" className="px-6 py-3 text-base">
                  Вкупно
                </th>
                <td className="px-6 py-3">
                  {formatNumberWithCommas(totalSum)} МКД
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  );
};

export default Presmetki;
