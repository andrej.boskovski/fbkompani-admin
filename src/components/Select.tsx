import React, { useState } from "react";

interface Props {
  className?: string;
  placeholder?: string;
  option1?: string;
  option2?: string;
  option3?: string;
  option4?: string;
  option5?: string;
  setSelectedOption: (value: string) => void;
  selectedOption?: string;
}

const Select = ({
  className,
  placeholder,
  option1,
  option2,
  option3,
  option4,
  option5,
  setSelectedOption,
  selectedOption,
}: Props) => {
  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedOption(event.target.value);
    selectedOption = event.target.value;
  };

  return (
    <select
      value={selectedOption}
      onChange={handleChange}
      className={`p-2 m-2 w-2/12 h-10 border border-gray-400 rounded-lg ${className}`}
    >
      <option disabled selected value="placeholder">
        {placeholder}
      </option>
      {option1 ? <option value={selectedOption}>{option1}</option> : null}
      {option2 ? <option value={selectedOption}>{option2}</option> : null}
      {option3 ? <option value={selectedOption}>{option3}</option> : null}
      {option4 ? <option value={selectedOption}>{option4}</option> : null}
      {option5 ? <option value={selectedOption}>{option5}</option> : null}
    </select>
  );
};

export default Select;
