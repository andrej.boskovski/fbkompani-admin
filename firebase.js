// Import the functions you need from the SDKs you need
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getDatabase, ref, set } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD3jDi8lFYS3KzkR7H1HJpTfLYIH_4l7Rs",
  authDomain: "fbkompani1.firebaseapp.com",
  databaseURL:
    "https://fbkompani1-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fbkompani1",
  storageBucket: "fbkompani1.appspot.com",
  messagingSenderId: "1038896396708",
  appId: "1:1038896396708:web:ea06c47a9cbf16f7cba931",
  measurementId: "G-TH99WDC6B9",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const reference = (db, "users/", userId);
const analytics = getAnalytics(app);

set(reference, {
  username: name,
  email: email,
  profilePicture: profilePicture,
});

writeUserData("Andrej", "andrea");
